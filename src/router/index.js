import Vue from 'vue'
import Router from 'vue-router'
import game from '@/components/game'
import home from '@/components/home'
import iachieve from '@/components/iachieve'
import loading from '@/components/loading'
import page from '@/components/page'
import choose from '@/components/choose'

Vue.use(Router)

export default new Router({

  mode: "hash",
  routes: [
    {
      path: '/',
      name: 'page',
      component: page
    },
    {
      path: '/loading',
      name: 'loading',
      component: loading
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/iachieve',
      name: 'iachieve',
      component: iachieve
    },
    {
      path: '/choose',
      name: 'choose',
      component: choose
    },
    {
      path: '/game',
      name: 'game',
      component: game
    },
    
  ]
})
