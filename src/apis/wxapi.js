import wx from 'weixin-js-sdk'
import  axios from  'axios'
import  qs from  'qs'
let baseUrl = 'https://iscgoldenweek.gypserver.com/server/'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

console.log(wx_conf)
wx.config({
    debug: false,
    appId: wx_conf.appId,
    timestamp: wx_conf.timestamp,
    nonceStr: wx_conf.nonceStr,
    signature: wx_conf.signature,
    jsApiList: [
        'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage'
    ]
});
//分享标题
var wxtitle = '求助我一臂之力，轻松拿走好礼';
//分享内容
var wxdesc = '你逛街我送礼，樟宜购物城就是这么疯狂';
//分享网址
var baseurl = '';
var shareurl = 'https://iscgoldenweek.gypserver.com/#/';
//分享图片
var imgUrl = 'https://iscgoldenweek.gypserver.com/static/img/share.jpg';


wx.ready(function() {
    var shareData = {
        title: wxtitle,
        desc: wxdesc,
        link: shareurl,
        imgUrl: imgUrl,
        success: function() { 
            let userInfo = JSON.parse(localStorage.getItem("userInfo"))
            console.log(userInfo)
            axios.post(baseUrl + 'SharingUpdateLimit',qs.stringify({openId:userInfo.openid})).then(res =>{
                console.log(res)
            })
            gtag('event', 'share_friend', {'event_category': 'isc','event_label': 'click'});
              
        },
        error: function() {}
    };
    var shareDataline = {
        title: wxtitle,
        desc: wxdesc,
        link: shareurl,
        imgUrl: imgUrl,
        success: function() {
            let userInfo = JSON.parse(localStorage.getItem("userInfo"))
            console.log(userInfo)
            axios.post(baseUrl + 'SharingUpdateLimit',qs.stringify({openId:userInfo.openid})).then(res =>{
                console.log(res)
            })
            gtag('event', 'share_moment', {'event_category': 'isc','event_label': 'click'});
              
        },
        error: function() {}
    };
    wx.onMenuShareAppMessage(shareData);
    wx.onMenuShareTimeline(shareDataline);

});