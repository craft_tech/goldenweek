import  axios from  'axios'
import  qs from  'qs'
let baseUrl = 'https://iscgoldenweek.gypserver.com/server/'
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

//获取产品列表
export function getCommodity(param) {
    return axios.post(baseUrl + 'GetCommodity',
    qs.stringify({sex:param}))
}

//剩余游戏次数
export function getGameTimes(param) {
    return axios.post(baseUrl + 'RemainingNum',
    qs.stringify({openId:param}))
}

//游戏分享次数
export function getShareTimes(param) {
    return axios.post(baseUrl + 'SharableNumber',
    qs.stringify({openId:param}))
}

//游戏分享
export function goShare(param) {
    return axios.post(baseUrl + 'SharingUpdateLimit',
    qs.stringify({openId:param}))
}

//获得个人最佳成绩
export function getBest(param) {
    return axios.post(baseUrl + 'BestPersonal',
    qs.stringify({openId:param}))
}

//保存游戏数据
export function SAVE(param) {
    return axios.post(baseUrl + 'Save',
    qs.stringify(param))
}

export function getTop10() {
    return axios.post(baseUrl + 'GetTop10')
}
//加入战队
export function joinTeam(param) {
    return axios.post(baseUrl + 'OceanQueue/JoinTeam',qs.stringify(param))
}

